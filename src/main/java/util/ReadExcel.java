package util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import javax.sound.midi.SysexMessage;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class ReadExcel {

	public static Object[][] getexcelData(String fileName) throws IOException {
		// TODO Auto-generated method stub
		//get workbook
				XSSFWorkbook wBook = new XSSFWorkbook("./data/"+fileName+".xlsx");
				//get sheet
				XSSFSheet sheet = wBook.getSheetAt(0);
				//get last row number 
				int lastRowNum = sheet.getLastRowNum();
				//get last column number
				short lastCellNum = sheet.getRow(0).getLastCellNum();
				
				Object [][] data= new Object[lastRowNum][lastCellNum]; //create a obj array
				//iterate for all the rows, it should start with i = 1, since 0 will be header
				for(int i = 1; i<=lastRowNum; i++) {
					//this will iterate all the row
					XSSFRow row = sheet.getRow(i);
					//iterate for all the columns, it should start with j = 0, since there will not be any header
					for(int j = 0; j<lastCellNum; j++) {
						//this will iterate all the column
						XSSFCell cell = row.getCell(j);
						//this will get the value from each cell
						String stringCellValue = cell.getStringCellValue();
						
						data [j-1][i] = stringCellValue;
						
						System.out.println(stringCellValue);
					}
				}
				wBook.close();
				return data;
			}
}