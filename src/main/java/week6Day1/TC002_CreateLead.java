package week6Day1;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import javax.sound.midi.SysexMessage;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import week4Day2.ProjectMethods;
import week4Day2.SeMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC002";
		testCaseDescription ="Create lead";
		category = "Smoke";
		author= "Cool";
	}
		
	@Test(invocationCount=2,timeOut=30000)
	public void createlead()  {
	
	login();
//	WebElement crmlink = locateElement("LinkText", "CRM/SFA");
//	click(crmlink);
	WebElement leadcreate = locateElement("LinkText", "Create Lead");
	click(leadcreate);
	WebElement compname = locateElement("id", "createLeadForm_companyName");
	type(compname, "Jill");
	WebElement frstname = locateElement("id", "createLeadForm_firstName");
	type(frstname, "Dinesh");
	String Expectdfrstname = frstname.getText();
	WebElement lastname = locateElement("id", "createLeadForm_lastName");
	type(lastname, "chella");
	WebElement drpdwn = locateElement("id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(drpdwn, "Employee");
	WebElement submit = locateElement("class","smallSubmit");
	click(submit);
	WebElement frstnamecreatd = locateElement("id", "viewLead_firstName_sp");
	String Actualfrstname = frstnamecreatd.getText();
	//Verify the Expected & Actual
	if(Actualfrstname.contains(Expectdfrstname))
	{
		System.out.println("Verification of First Name is Success");
	}else
		System.out.println("Verification of First Name is Failed");
	

}
}
