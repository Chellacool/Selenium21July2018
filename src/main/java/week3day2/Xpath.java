package week3day2;

import org.openqa.selenium.chrome.ChromeDriver;


public class Xpath {



	public static void main(String[] args) {
		// TODO Auto-generated method stub


		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("http://leaftaps.com/opentaps/");
		//enter credential
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//Hit Submit button
		driver.findElementByClassName("decorativeSubmit").click();
		//Click a link in the page
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads
		driver.findElementByLinkText("Leads").click();
		//Click on Find Leads
		driver.manage().timeouts().implicitlyWait(30, TimeUnit);
		driver.findElementByLinkText("Find Leads").click();
		//Enter first name value
		driver.findElementByXPath("(//input[@class=' x-form-text x-form-field ']").sendKeys("s");
		//Click on find lead button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//click on the first link
		driver.findElementByXPath("(//div[@class=\"x-grid3-cell-inner x-grid3-col-partyId\"]/a)[1]").click();
				

	}

}
