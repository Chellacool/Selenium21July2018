package projectDay;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class FBSearchTestLeaf extends Facebook_Method {

	String EnterSearch = "TestLeaf";
	
	@Test
	public void search() throws InterruptedException {
		
		//enter into Search button
		WebElement Search = locateElement("xpath", "//input[@class='_1frb\']");
		//Type Test Leaf
		type(Search, "TestLeaf");
		//Click on the search button after typing
		WebElement Searchbutton = locateElement("xpath", "//button[@class='_42ft _4jy0 _4w98 _4jy3 _517h _51sy _4w97']");
		click(Searchbutton);
		//Wait for loading
		Thread.sleep(3000);
		//Verify the value of entered value
		WebElement VerifyClick = locateElement("xpath", "//div[@class='_52eh _5bcu']");
		//To verify and print the the entered value match
		if (VerifyClick.equals(EnterSearch)){
			System.out.println("Entered Search is verified");
		}
		//click on the like button				
		WebElement likebutton = locateElement("xpath", "(//button[@type='submit'])[2]");
		String liketext = likebutton.getText();
		if (liketext == "Like") {
		click(likebutton);
		}
		//if already clicked sysout
		else {
		System.out.println("This page TestLeaf is already liked");
		}
		//Click on the page
		click(VerifyClick);
		Thread.sleep(3000);
		//click on the notification
		WebElement Noticlick = locateElement("xpath","(//div[@class='_2n_9'])[2]");
		click(Noticlick);
		//verifying the title
		boolean verfyTitl = verifyTitle("TestLeaf - Home");
		if(verfyTitl) {
			System.out.println("Title is Verified");
		}else
			System.out.println("Title is not Verified");
		
		
	}

}
