package week4day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertFrame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		//Maximize the screen
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//Get in to the frame
		driver.switchTo().frame("iframeResult");		
		// try it button
		driver.findElementByXPath("//button[text()='Try it']").click();		
		//get into alert
		driver.switchTo().alert().sendKeys("Dinesh");
		driver.switchTo().alert().accept();
		//get in to frame again
		//driver.switchTo().frame("iframeResult");	
		//get the text from the frame
		String text = driver.findElementByXPath("//p[@id='demo']").getText();
		// print the text value
		System.out.println(text);
		

	}

}
