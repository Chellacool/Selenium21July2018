package week4day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnMouseHover {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("http://jqueryui.com/draggable/");
		//Maximize the screen
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//find the frame 
		driver.switchTo().frame(0);
		//find the draggable element
		WebElement drag = driver.findElementByXPath("//div[@id='draggable']/p");
		// perform the mouse action
		Actions obj1=new Actions(driver);
		obj1.dragAndDropBy(drag, 100, 100).perform();
		
		
		

	}

}
