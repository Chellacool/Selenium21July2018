package week4Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class TC002 extends ProjectMethods {
	
	@Test
	public void createlead() {

		//login();
		WebElement crmlink = locateElement("LinkText", "CRM/SFA");
		click(crmlink);
		WebElement leadcreate = locateElement("LinkText", "Create Lead");
		click(leadcreate);
		WebElement compname = locateElement("id", "createLeadForm_companyName");
		type(compname, "Jill");
		WebElement frstname = locateElement("id", "createLeadForm_firstName");
		type(frstname, "Dinesh");
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, "chella");
		WebElement submit = locateElement("class","smallSubmit");
		WebElement drpdwn = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(drpdwn, "Employee");
		click(submit);
		WebElement lead = locateElement("LinkText","Leads");
		click(lead);
		WebElement mrglead = locateElement("LinkText","Merge Leads");
		click(mrglead);
		WebElement frmlead = locateElement("id","//img[@id='ext-gen595']");
		click(frmlead);
		switchToWindow(2);
		WebElement firstname = locateElement("class","firstName");
		type(firstname, "dinesh");
		WebElement lstname = locateElement("class","(//input[@id='ext-gen29']");
		type(lstname, "chella");
		WebElement fndlead = locateElement("id","(//button[@id='ext-gen114']");
		click(fndlead);
		WebElement flead = locateElement("LinkText","ext-gen358");
		click(flead);
		
		
		
		
		
		
		

	}


	}

